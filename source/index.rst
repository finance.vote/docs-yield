.. yield.vote documentation master file, created by
   sphinx-quickstart on Fri Jun 25 13:47:32 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to yield.vote's documentation!
======================================
`yield.vote`_ is a liquidity mining and staking platform that provides users of token ecosystems with yield opportunities in return for supporting the creation of decentralised markets. Yield has tuneable governance parameters that can be modified by a DAO.

.. _yield.vote: https://yield.factorydao.org/

.. image:: ./images/yield_hz_b.png
   :height: 50px
   :alt: docs-yield missing
   :align: center
   :target: https://yield.factorydao.org/


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   /content/about
   /content/setting_up
   /content/token
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Our products
===============

.. |A| image:: ./images/launch_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/launch/en/latest/

.. |B| image:: ./images/bank_icon_dark.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/bank/en/latest/

.. |I| image:: ./images/influence_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/influence/en/latest/

.. |M| image:: ./images/markets_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/markets/en/latest/

.. |MI| image:: ./images/mint_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/mint/en/latest/ 

* |A| `launch <https://financevote.readthedocs.io/projects/launch/en/latest/>`_
* |B| `bank <https://financevote.readthedocs.io/projects/bank/en/latest/>`_
* |I| `influence <https://financevote.readthedocs.io/projects/influence/en/latest/>`_
* |M| `markets <https://financevote.readthedocs.io/projects/markets/en/latest/>`_
* |MI| `mint <https://financevote.readthedocs.io/projects/mint/en/latest/>`_


Back to main page
------------------
.. |H| image:: ./images/financevote_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/en/latest/

* |H| `Home <https://financevote.readthedocs.io/en/latest/>`_
