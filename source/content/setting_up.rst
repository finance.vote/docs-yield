Setting up
-----------------
Clone following repository:

* https://gitlab.com/finance.vote/yield.vote

Prerequisites:

* Created an account on GitLab via invite
* Code editor installed

1. In the yield.vote terminal::

    git checkout develop

2. In the main yield.vote folder create **.env.local** file and put in there::

    REACT_APP_MAINNET_PROVIDER_URL='wss://mainnet.infura.io/ws/v3/822386a234764efbb051a77ae93c1e79'
    REACT_APP_ROPSTEN_PROVIDER_URL='wss://ropsten.infura.io/ws/v3/822386a234764efbb051a77ae93c1e79'
    REACT_APP_BSC_PROVIDER_URL='https://bsc-dataseed.binance.org/'
    REACT_APP_LOCAL_PROVIDER_URL='ws://localhost:8545'

3. Install dependencies::

    npm install

4. Run application::

    npm start
