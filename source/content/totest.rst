Testing
========
| In purpose of testing yield, you'll have to go into 
| ``./src/partners.json``

copy and paste below code into finance key depositTokens part::

  "depositTokens": [
       "0x96335e7CdbCD91fB33C26991b00cc13a87A811b9",
       "0xF7eF90B602F1332d0c12cd8Da6cd25130c768929",
       "0xBFe5C40d16Be2EC652A627b1c2922B076DE267fD",
       "0x485345705b85A416fA1276fbcA7FB81F3Ebd9d13",
       "0xf5a9C4E4FC81A9F628058C18322Fd1d7F51452a0"
      ],

do the same for iTrust key depositTokens part::

  "depositTokens": [
       "0xCd7a2b621e297a6A466f2De472462C296A4EF258",
       "0x5615187acc2d65f078c36e07f06ed85225a5dd9d",
       "0xC3795a945923914f0e5e4CF45c68F523FE835238",
       "0xA34CAB81aa58CeF453A73Cdfe5600b9b699F9d66",
       "0x46a0b72f44c0B380e85BE1215E3F7CAb1efaF046"
      ],
